<?php

namespace Alketta\Modele;
use PDO;
/**
 * Permet de créer un objet correspondant à la relation EST_PRESENT.
 */


// Plusieurs gènes appartiennent à un tissu.
class EstPresent {

	// Variable

	// Correspond à l'identifiant d'un gène
	public $idGene;

	// Correspond à l'identifiant d'un tissu
	public $idTissue;

	// Correspond à au taux d'expression d'un gène au sein d'un tissu
	public $tauxExpression;

	// Constrctor
	function __construct($idGene, $idTissue, $tauxExpression)
	{
		$this->idGene = $idGene;
		$this->idTissue = $idTissue;
		$this->tauxExpression = $tauxExpression;
	}

	// Getter
	public function getIdGene(){
		return $this->idGene;
	}

	public function getIdTissue(){
		return $this->idTissue;
	}

	public function getTauxExpression(){
		return $this->tauxExpression;
	}

	public static function geneExpressionTissu($idGene, $idTissu){
		$tabTaux = [];
		$connect = Connection::connectBd();
		$requete = $connect->prepare('SELECT * FROM EST_PRESENT WHERE idGene = :idGene AND idTissue = :idTissu');
		$requete->bindValue(':idGene', $idGene, PDO::PARAM_INT);
		$requete->bindValue(':idTissu', $idTissu, PDO::PARAM_INT);
		$requete->execute();
		if ($row = $requete->fetch()) {
			$tabTaux = new EstPresent($row['idGene'], $row['idTissue'], $row['tauxExpression']);
		}
		else {
			$tabTaux = new EstPresent($idGene, $idTissu, 0);
		}
		return $tabTaux;
	}

}
