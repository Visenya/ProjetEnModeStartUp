<? php

/**
 * Permet de créer un objet correspondant à la relation EST_PRESENT.
 */


// Un gène appartient à un chromosone.
class EstLocalise{

	// Variable

	// Correspond à l'identifiant d'un gène
	private $idGene;

	// Correspond à l'identifiant d'un chromosome
	private $idChromosome;

	// Constructor
	function __construct($idGene, $idChromosome)
	{
		$this->idGene = $idGene;
		$this->idChromosome = $idChromosome;
	}

	// Getter
	public function getIdGene(){
		return $this->idGene;
	}

	public function getIdTissue(){
		return $this->idChromosome;
	}

}
