<?php

namespace Alketta\Modele;
use PDO;

/**
 * Permet de créer un objet correspondant à l'entité TISSU.
 */

class Tissu {

	// Variable

	// Correspond à l'identifiant d'un gène
	public $idTissu;

	// Correspond à l'identifiant d'un tissu
	public $nomTissu;

	// Constructor
	function __construct($idTissu, $nomTissu)
	{
		$this->idTissu = $idTissu;
		$this->nomTissu = $nomTissu;
	}

	// Getter
	public function getIdTissu(){
		return $this->idTissu;
	}

	public function getNomTissu(){
		return $this->nomTissu;
	}

	public static function tousLesTissus(){
		$tabTissu = [];
		$connect = Connection::connectBd();
		$requete = $connect->query('SELECT * FROM TISSUE');
		foreach ($requete->fetchAll() as $value) {
			$tabTissu[] = new Tissu($value['idTissue'], $value['nomTissue']);
		}
		return $tabTissu;
	}

	public static function rechercheTissu($idTissu){
		$connect = Connection::connectBd();
		$requete = $connect->prepare('SELECT * FROM TISSUE WHERE idTissue = :idTissu');
		$requete->bindValue(':idTissu', $idTissu, PDO::PARAM_INT);
		$requete->execute();
		foreach ($requete->fetchAll() as $value) {
			$tabTissusTrouves = new Tissu($value['idTissue'], $value['nomTissue']);
		}
		return $tabTissusTrouves;
	}

	public static function rechercheTissuParNom($nomTissu){
		$connect = Connection::connectBd();
		$requete = $connect->prepare('SELECT * FROM TISSUE WHERE nomTissue = :nomTissu');
		$requete->bindValue(':nomTissu', $nomTissu);
		$requete->execute();
		foreach ($requete->fetchAll() as $value) {
			$tabTissusTrouves = new Tissu($value['idTissue'], $nomTissu);
		}
		return $tabTissusTrouves;
	}

}
