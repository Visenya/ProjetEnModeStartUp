<?php

namespace Alketta\Modele;
use PDO;

/**
 * Permet de créer un objet correspondant à l'entité GENE.
 */

class Gene {

	// Variable

	// Correspond à l'identifiant d'un gène
	public $idGene;

	// Correspond au code uniGene
	public $uniGene;

	// Correspond au code refSeq
	public $refSeq;

	// Correspond au code GeneSymbol
	public $geneSymbol;

	// Constructor
	function __construct($idGene, $uniGene, $refSeq, $geneSymbol){
		$this->idGene = $idGene;
		$this->uniGene = $uniGene;
		$this->refSeq = $refSeq;
		$this->geneSymbol = $geneSymbol;
	}

	// Getter
	public function getIdGene(){
		return $this->idGene;
	}

	public function getUniGene(){
		return $this->uniGene;
	}

	public function getRefSeq(){
		return $this->refSeq;
	}

	public function getGeneSymbol(){
		return $this->geneSymbol;
	}

	public static function tousLesGenes(){
		$tabGene = [];
		$connect = Connection::connectBd();
		$requete = $connect->query('SELECT * FROM GENE');
		foreach ($requete->fetchAll() as $value) {
			$tabGene[] = new Gene($value['idGene'], $value['uniGene'], $value['refSeq'], $value['geneSymbol']);
		}
		return $tabGene;
	}

	public static function rechercheGene($idGene){
		$connect = Connection::connectBd();
		$requete = $connect->prepare('SELECT * FROM GENE WHERE idGene = :idGene');
		$requete->bindValue(':idGene', $idGene, PDO::PARAM_INT);
		$requete->execute();
		foreach ($requete->fetchAll() as $value) {
			$tabGenesTrouvés = new Gene($value['idGene'], $value['uniGene'], $value['refSeq'], $value['geneSymbol']);
		}
	return $tabGenesTrouvés;
	}

	public static function rechercheGeneParNom($nomGene)
	{
		$connect = Connection::connectBd();
		$requete = $connect->prepare('SELECT * FROM GENE WHERE uniGene = :nomGene');
		$requete->bindValue(':nomGene', $nomGene);
		$requete->execute();
		foreach ($requete->fetchAll() as $value) {
			$tabGenesTrouvés = new Gene($value['idGene'], $value['uniGene'], $value['refSeq'], $value['geneSymbol']);
		}
	return $tabGenesTrouvés;
	}

}
