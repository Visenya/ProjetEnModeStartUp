<?php

namespace Alketta\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Alketta\Modele\Tissu;
use Alketta\Modele\Gene;
use Alketta\Modele\EstPresent;
use League\Csv\Reader;
use League\Csv\Statement;

class EntrerTauxExpression {

  // Fonction permettant de recuperer les info nécessaire pour afficher page EntrerTauxExpression

  public function affichePageEntrerTauxExpression(Request $request, Application $app){
    $tabTissus = Tissu::tousLesTissus();
    $tabGenes = Gene::tousLesGenes();
    return $app['twig']->render('EntrerTauxExpression.twig', array(
        'tabGenes' => $tabGenes, 'tabTissus' => $tabTissus
    ));
  }

  // Fonction permmettant de récupérer les informations du csv

  public function recupererInfoCSV(Request $request, Application $app){
    $tabTissus = Tissu::tousLesTissus();
    $tabGenes = Gene::tousLesGenes();
    $lap = 0;
    /** Ici, on récupère le fichier envoyé */
    $CSVResultStat = $request->files->get('fichier');

    if (!in_array($CSVResultStat->getMimeType(), ['text/csv', 'application/vnd.ms-excel', 'text/plain'])) {
      throw new \Exception("The file is not a CSV file");

    }
    /** Suite à l'exécution du programme, on teste l'existance et la lisibilité du fichier
    de transfert de données **/
    if (!file_exists($CSVResultStat)) {
      throw new \Exception("Erreur fichier n'existe pas");
    }
    if (!is_readable($CSVResultStat)) {
      throw new \Exception("Le fichier n'est pas lisible");
    }

   /** On instancie le lecteur de CSB+V */
    $csv = Reader::createFromPath($CSVResultStat);

    /** On définit le délimiteur de nos colonnes */
    $csv->setDelimiter(';');

    /** On initialise nos tableaux de tissus et de gènes */
    $resultats = [];
    $genes = [];

    /** Pour chaque ligne du fichier CSV */
    foreach ($csv->fetchAll() as $row) {
      $resultat['gene'] = $row[0];
      $gene = (int) Gene::rechercheGeneParNom($row[0])->getIdGene();
      if (!in_array($gene, $genes)) {
        $genes[$gene] = $resultat['gene'];
      }
      $resultat['idgene'] = $gene;
      $resultat['tissu'] = $row[1];
      $idTissu = (int) Tissu::rechercheTissuParNom($row[1])->getIdTissu();
      $resultat['txExpressionFichier'] = (float) $row[2];
      $resultat['txExpressionDB'] = (float) EstPresent::geneExpressionTissu($gene, $idTissu)->getTauxExpression();
      $resultats[] = $resultat;
    }
    // var_dump($resultats);
    // var_dump($genes);



    return $app['twig']->render('EntrerTauxExpression.twig', [
        'resultats' => $resultats,
        'copieResultats' => json_encode($resultats),
        'genes' => $genes,
    ]);
  }

}
