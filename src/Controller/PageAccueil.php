<?php

namespace Alketta\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class PageAccueil {

  // Fonction qui permet d'afficher la page Accueil

  public function affichePageAccueil(Request $request, Application $app){

    return $app['twig']->render('PageAccueil.twig');
  }
}
