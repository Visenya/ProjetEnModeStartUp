<?php

namespace Alketta\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Alketta\Modele\Tissu;
use Alketta\Modele\Gene;
use Alketta\Modele\EstPresent;


class PageGraphiqueEtoile {

  //  fonction redirigeant la selection de l'entête vers la page en passant les parametre
  //  obtenus des requete tousLesTissus du model tissus et touslesGenes du model Gene

  public function affichePageGraphiqueEtoile(Request $request, Application $app){
    $tabTissus = Tissu::tousLesTissus();
    $tabGenes = Gene::tousLesGenes();
    return $app['twig']->render('PageGraphiqueEtoile.twig', array(
        'tabGenes' => $tabGenes, 'tabTissus' => $tabTissus
    ));
  }

  // Fonction permettant de récupérer les infos lié à la création du graphiqueEtoile

  public function graphiqueEtoile(Request $request, Application $app){
    $tabTissus = Tissu::tousLesTissus();
    $tabGenes = Gene::tousLesGenes();

    $tissus = $request->get('tissu');
    $uniGene = $request->get('UniGene');
    $RefSeq = $request->get('RefSeq');
    $geneSymbol = $request->get('geneSymbol');

    $tabObjectGene = [];
    foreach ($uniGene as $idValue) {
      $tabObjectGene[] = Gene::rechercheGene($idValue);
    }

    $tabObjectTissu = [];
    foreach ($tissus as $idValue) {
      $tabObjectTissu[] = Tissu::rechercheTissu($idValue);
    }

    foreach ($tabObjectGene as $gen) {
      $thegene = $gen->getIdGene();
      foreach ($tabObjectTissu as $tis) {
        $thetissu = $tis->getIdTissu();
        # Suppression tableau double dimension $tabTaux[$thegene][$thetissu]
        $testTaux = EstPresent::geneExpressionTissu($thegene, $thetissu);
        if ($testTaux != NULL) {
          $tabTaux[] = $testTaux;
        }
      }
    }

    return $app['twig']->render('PageGraphiqueEtoile.twig', array('tabTaux' => json_encode($tabTaux),'tabObjectTissu'=>json_encode($tabObjectTissu),'tabObjectGene'=>json_encode($tabObjectGene), 'tabTissus' => $tabTissus, 'tabGenes' => $tabGenes ));

  }
}
