<?php

namespace Alketta\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Alketta\Modele\Tissu;
use Alketta\Modele\Gene;
use Alketta\Modele\EstPresent;

class PageTissus {

  // Fonction récupérant les infos pour créer la PageTissus

  public function affichePageTissus(Request $request, Application $app){
    $tabTissus = Tissu::tousLesTissus();
    $tabGenes = Gene::tousLesGenes();
    return $app['twig']->render('PageTissue.twig', array(
        'tabGenes' => $tabGenes, 'tabTissus' => $tabTissus
    ));
  }

  // Fonction récupérant les infos pour créer le graphique

  public function graphiqueTissus(Request $request, Application $app){
    $tabTissus = Tissu::tousLesTissus();
    $tabGenes = Gene::tousLesGenes();

    $tissus = $request->get('tissu');
    $uniGene = $request->get('UniGene');
    $refSeq = $request->get('RefSeq');

    $tabObjectGene = [];

    if (count($uniGene)!=0) {
      foreach ($uniGene as $idValue) {
        $tabObjectGene[] = Gene::rechercheGene($idValue);
      }
    } else {
      foreach ($refSeq as $idValue) {
        $tabObjectGene[] = Gene::rechercheGene($idValue);
      }
    }

    $tabObjectTissu = [];
    foreach ($tissus as $idValue) {
      $tabObjectTissu[] = Tissu::rechercheTissu($idValue);
    }

    foreach ($tabObjectGene as $gen) {
      $thegene = $gen->getIdGene();
      foreach ($tabObjectTissu as $tis) {
        $thetissu = $tis->getIdTissu();
        # Suppression tableau double dimension $tabTaux[$thegene][$thetissu]
        $testTaux = EstPresent::geneExpressionTissu($thegene, $thetissu);
        if ($testTaux != NULL) {
          $tabTaux[] = $testTaux;
        }
      }
    }

    return $app['twig']->render('PageTissue.twig', array('tabTaux' => json_encode($tabTaux),'tabObjectTissu'=>json_encode($tabObjectTissu),'tabObjectGene'=>json_encode($tabObjectGene), 'tabTissus' => $tabTissus, 'tabGenes' => $tabGenes ));

  }


}
