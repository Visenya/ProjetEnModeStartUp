<?php

namespace Alketta\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class ControllerHelp {

  // Fonction permettant l'affichage de la page web 

  public function affichePageHelp(Request $request, Application $app){
    return $app['twig']->render('Help.twig');
  }
}
