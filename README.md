# Alketta



### Objectif

1. Réalisation de profil d'expression de gènes selon les tissus.
2. Observer des différences d'expression selon tissus.
3. Pouvoir comparer le niveau d'expression, pour un gène, d'un malade à un profil dit "de référence".


### Installation

* Vérifier la comptabilité avec les langages utilisés : `PHP7`, `CSS3`, `HTML5`, `JavaScript`.

* Installation d'un serveur `XAMP` ou seulement d'`Apache2`.

* SGBD : `PHPMyAdmin`

* Framework : `Silex`

* Template : `Twig`

* Compléter le script `infoCo.php` recensant les informations liées à la connexion du SGBD


### Running

Lancez votre serveur `Apache2` ou  `php -S localhost:8080`


### Using

http://localhost:xxxx
