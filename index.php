<?php

require_once __DIR__.'/vendor/autoload.php';
session_start();

$app = new Silex\Application();

// Permet d'avoir les informations lors du débuggage
$app['debug'] = true;

// Lancer l'Application

// -> Graphique en developpement

// $app->get('/PageGraphiqueEtoile', 'Alketta\\Controller\\PageGraphiqueEtoile::affichePageGraphiqueEtoile');
//
// $app->post('/PageGraphiqueEtoile', 'Alketta\\Controller\\PageGraphiqueEtoile::graphiqueEtoile');

// -> Help

$app->get('/Help', 'Alketta\\Controller\\ControllerHelp::affichePageHelp');

// -> Analyse

$app->get('/EntrerTauxExpression', 'Alketta\\Controller\\EntrerTauxExpression::affichePageEntrerTauxExpression');

$app->post('/recupererInfoCSV','Alketta\\Controller\\EntrerTauxExpression::recupererInfoCSV');

// -> Visualisation

  // First
$app->get('/PageTissue', 'Alketta\\Controller\\PageTissus::affichePageTissus');

$app->post('/PageTissue', 'Alketta\\Controller\\PageTissus::graphiqueTissus');

  // Second

$app->get('/Visualisation', 'Alketta\\Controller\\Visualisation::affichePageVisualisation');

$app->post('/Visualisation', 'Alketta\\Controller\\Visualisation::graphiqueEtoile2');

// -> Acceuil

$app->get('/', 'Alketta\\Controller\\PageAccueil::affichePageAccueil');

// -> Mise en Place Système

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/Vue',
));

$app['asset_path'] = '/Vue/assets';

$app->run();
